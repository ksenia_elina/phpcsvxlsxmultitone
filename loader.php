<?php
include "csv.php";
include "xlsx.php";
  class DB
  {
      protected static $instance = array();
    protected static $data = [
    array("firstname" => "Mary", "lastname" => "Johnson", "age" => 25),
    ["firstname" => "Amanda", "lastname" => "Miller", "age" => 18],
    ["firstname" => "James", "lastname" => "Brown", "age" => 31],
    ["firstname" => "Patricia", "lastname" => "Williams", "age" => 7],
    ["firstname" => "Michael", "lastname" => "Davis", "age" => 43],
    ["firstname" => "Sarah", "lastname" => "Miller", "age" => 24],
    ["firstname" => "Patrick", "lastname" => "Miller", "age" => 27]
  ];
  
      private function __construct($key)
      {
          //var_dump($key);
          if($key == 'csv')
          {
              //var_dump($key);
              self::$instance[$key] = new csv(self::$data);
          }
          else
              
              {
                   self::$instance[$key] = new xlsx(self::$data);
              }
     }
     public static function getInstance($key)
     {

         if (is_null(self::$instance[$key])) {
            // var_dump($key);
             self::$instance[$key] = new self($key);
         }
         return self::$instance[$key];
     }

     private function __clone()
     {
     }
     private function __wakeup()
     {
     }
}
$sql = 'some sql';
//$mysql = DB::getInstance('csv');
//$mysql->query($sql);
$sqlite = DB::getInstance('xlsx');
//$sqlite->query($sql);
?>